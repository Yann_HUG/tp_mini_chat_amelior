<?php
session_start();
?>

<!doctype html>

<html>
	<head>
        <?php
         try
        {
            $bdd = new PDO('mysql:host=localhost;dbname=tp_mini_chat;charset=utf8', 'root', ''); // Appel de la base de données + verification erreur
        }
        catch(Exception $e)
        {
            die('Erreur : '.$e->getMessage());
        }
        ?> 
        
		<title>Mini Chat</title>
		<meta charset="UTF-8">
        <link href="style.css" rel="stylesheet" />
                
	</head>
        
	<body>

        <h1>Mini Chat</h1>
        
        <h3>Bienvenue sur le Mini Chat.</h3>
        <h4>Pour ecrire un message merci d'entrer un pseudo et votre texte</h4>
        <h5>Attention chaque champs est limité à 255 caractères.</h5>

		<form action="mini_chat_post.php" method="post"> <!-- Création du formulaire -->
        <p><label for="pseudo">Pseudo</label> : <input type="text" name="pseudo" value="<?php if(isset($_COOKIE['pseudo']))
        {
            echo $_COOKIE['pseudo']; // Mise en mémoire du pseudo
        }
        
        ?>" /><br />
        <label for="message">Message</label> :  <input type="text" name="message"/><br />
        <input type="submit" value="Envoyer" /></p>
    </form>
        
        
        <form action="mini_chat.php" method="refresh"><input type="submit" value="Rafraichir" /></form> <!-- Création du bouton rafraichir -->
        

        <div id="message"><br/>
        <?php
                // affichage du message avec mise en forme de la date
            $reponse = $bdd->query('SELECT pseudo, message, DATE_FORMAT(date_creation, "%d/%m/%Y %Hh%imin%ss") AS date_creation FROM mini_chat ORDER BY id DESC LIMIT 0, 10');
            
            while ($donnees = $reponse->fetch())
            {
        ?>
                <p id="separation">[<?php echo $donnees['date_creation'] ?>] <?php echo htmlspecialchars($donnees['pseudo']) ?> :  <?php echo htmlspecialchars($donnees['message']) ?> </p><br/>
        <?php
            }
            $reponse->closeCursor();
        ?>
         

        </div>
        
	</body>
</html>